# test-datasource

This is a datasource that creates synthetic signals that can be used for testing Octopy and potentially other parts of the system. It generates a wide variety of signal types.