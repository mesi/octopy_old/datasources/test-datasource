from lib.links.base_link import BaseLink


class TestLink(BaseLink):
    def deserialize(self, conf):
        super().deserialize(conf)
        self.link_type = 'test'
