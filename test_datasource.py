#!/usr/bin/python3
import sys
import time
import json
import logging
from os import path
import argparse
import traceback
import math
import random
from lib.octopyapp.octopyapp import OctopyApp
from lib.links import links
from test_link import TestLink

APP_ID = 'test datasource'
LOG_LEVEL = logging.INFO

FUNCTION_SINE_TOPIC = "function/sine"
FUNCTION_SAW_TOPIC = "function/saw"
FUNCTION_SQUARE_TOPIC = "function/square"
FUNCTION_NOISE_TOPIC = "function/noise"
FUNCTION_COMBINED_TOPIC = "function/combined"

FAST_SINE_TOPIC = "function/fast_sine"

TRIGGER_1_TOPIC = "trigger/1"
TRIGGER_5_TOPIC = "trigger/5"
TRIGGER_10_TOPIC = "trigger/10"
TRIGGER_60_TOPIC = "trigger/60"

FUNCTION_SINE_FREQUENCY = 0.02
FUNCTION_SAW_FREQUENCY = 0.05
FUNCTION_SQUARE_FREQUENCY = 0.1

TRIGGER_1_INTERVAL = 1.0
TRIGGER_5_INTERVAL = 5.0
TRIGGER_10_INTERVAL = 10.0
TRIGGER_60_INTERVAL = 60.0

STATUS_PUBLISH_INTERVAL = 2
FUNCTION_UPDATE_INTERVAL = 0.5

FAST_SINE_DATA_POINTS = 500


class TestDatasource(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.id = 'test_datasource'
        self.links = {}
        self.source_prefix = path.join(self.config['mqtt']['prefix'], self.id)
        self.function_counter = 0
        self.create_links()


    def start(self):
        super().start(start_loop = False)
        # Wait for MQTT connection
        logging.info("Waiting for MQTT connection")
        while not self.connected:
            self.mqtt_client.loop(0.1)
            self.mqtt_client.loop_misc()
        tick = 0.01  # 10 Hz refresh rate
        last_status_publish = time.time() - STATUS_PUBLISH_INTERVAL
        last_function_update = time.time() - FUNCTION_UPDATE_INTERVAL
        last_mqtt_misc_update = time.time()

        last_trigger_1_update = time.time() - TRIGGER_1_INTERVAL
        last_trigger_5_update = time.time() - TRIGGER_5_INTERVAL
        last_trigger_10_update = time.time() - TRIGGER_10_INTERVAL
        last_trigger_60_update = time.time() - TRIGGER_60_INTERVAL
        # MQTT main loop
        while True:
            try:
                self.mqtt_client.loop(tick)
                current_time = time.time()
                if current_time >= last_status_publish + STATUS_PUBLISH_INTERVAL:
                    # Publish datasource status and links regularly
                    last_status_publish = current_time
                    self.publish_status()
                if current_time >= last_function_update + FUNCTION_UPDATE_INTERVAL:
                    # Fast update
                    last_function_update = current_time
                    self.function_generator()
                    self.fast_data()
                if current_time >= last_mqtt_misc_update + 2:
                    # Let the MQTT library do its housekeeping regularly
                    last_mqtt_misc_update = current_time
                    self.mqtt_client.loop_misc()
                # Trigger functions. Ugly to place it here, should probably be put into separate function.
                if current_time >= last_trigger_1_update + TRIGGER_1_INTERVAL:
                    last_trigger_1_update = current_time
                    if self.connected:
                        self.publish(self.links['trigger_1'].value_topic, json.dumps([True, {'t': current_time}]))
                if current_time >= last_trigger_5_update + TRIGGER_5_INTERVAL:
                    last_trigger_5_update = current_time
                    if self.connected:
                        self.publish(self.links['trigger_5'].value_topic, json.dumps([True, {'t': current_time}]))
                if current_time >= last_trigger_10_update + TRIGGER_10_INTERVAL:
                    last_trigger_10_update = current_time
                    if self.connected:
                        self.publish(self.links['trigger_10'].value_topic, json.dumps([True, {'t': current_time}]))
                if current_time >= last_trigger_60_update + TRIGGER_60_INTERVAL:
                    last_trigger_60_update = current_time
                    if self.connected:
                        self.publish(self.links['trigger_60'].value_topic, json.dumps([True, {'t': current_time}]))
            except RuntimeError:
                self.halt('Runtime error')
            except KeyboardInterrupt:
                self.halt('Keyboard interrupt')
            except BaseException as err:
                logging.error(err)
                if LOG_LEVEL == logging.DEBUG:
                    traceback.print_exc()


    def halt(self, reason):
        logging.debug('Exiting: ' + reason)
        # TODO: Publish empty status
        self.mqtt_client.loop()
        self.stop()
        sys.exit(0)


    def publish_status(self, empty = False):
        links = []
        for link in self.links.values():
            links.append(link.serialize())
        structure = {
            'links': links,
            'datasource id': self.id,
            'type': 'system',
            'status': 'connected'
        }
        payload = json.dumps(structure, indent = 4)
        topic = path.join(self.topics['datasource']['status'], self.id)
        logging.debug(f"Publishing status for system datasource")
        self.publish(topic, payload)


    def function_generator(self):
        self.function_counter += 1
        sine = math.sin(self.function_counter * FUNCTION_UPDATE_INTERVAL * FUNCTION_SINE_FREQUENCY * math.pi * 2) / 2 + 0.5
        saw = (self.function_counter * FUNCTION_UPDATE_INTERVAL * FUNCTION_SAW_FREQUENCY) % 1
        square = 0 if (self.function_counter * FUNCTION_UPDATE_INTERVAL * FUNCTION_SQUARE_FREQUENCY) % 1 < 0.5 else 1
        noise = random.randint(0, 1000000) / 1000000.0
        combined = {
            'sine': sine,
            'saw': saw,
            'square': square,
            'noise': noise
        }
        ts = time.time()
        logging.debug(f"Function Generator - SINE: {sine}, SAW: {saw}, SQUARE: {square}, NOISE: {noise}")
        if self.connected:
            self.publish(self.links['sine'].value_topic, json.dumps([sine, {'t': ts}]))
            self.publish(self.links['saw'].value_topic, json.dumps([saw, {'t': ts}]))
            self.publish(self.links['square'].value_topic, json.dumps([square, {'t': ts}]))
            self.publish(self.links['noise'].value_topic, json.dumps([noise, {'t': ts}]))
            self.publish(self.links['combined'].value_topic, json.dumps([combined, {'t': ts}]))


    def fast_data(self):
        data = [math.sin(x / FAST_SINE_DATA_POINTS * math.pi * 2) for x in range(0, FAST_SINE_DATA_POINTS)]
        ts = time.time()
        logging.debug(f"Fast sine - {FAST_SINE_DATA_POINTS} data points")
        if self.connected:
            self.publish(self.links['fast_sine'].value_topic, json.dumps([data, {'t': ts}]))


    def create_links(self):
        self.links['sine'] = TestLink({
            'value topic': path.join(self.source_prefix, FUNCTION_SINE_TOPIC),
            'id': 'sine',
            'description': f"Sine wave\n\nA synthetic sine wave signal of {FUNCTION_SINE_FREQUENCY} Hz",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'min': 0,
                'max': 1
            }
        })
        self.links['saw'] = TestLink({
            'value topic': path.join(self.source_prefix, FUNCTION_SAW_TOPIC),
            'id': 'saw',
            'description': f"Saw wave\n\nA synthetic saw wave signal of {FUNCTION_SAW_FREQUENCY} Hz",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'min': 0,
                'max': 1
            }
        })
        self.links['square'] = TestLink({
            'value topic': path.join(self.source_prefix, FUNCTION_SQUARE_TOPIC),
            'id': 'square',
            'description': f"Square wave\n\nA synthetic square wave signal of {FUNCTION_SQUARE_FREQUENCY} Hz",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'min': 0,
                'max': 1
            }
        })
        self.links['noise'] = TestLink({
            'value topic': path.join(self.source_prefix, FUNCTION_NOISE_TOPIC),
            'id': 'noise',
            'description': f"Noise\n\nLow frequency pink noise",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'double',
                'min': 0,
                'max': 1
            }
        })

        self.links['combined'] = TestLink({
            'value topic': path.join(self.source_prefix, FUNCTION_COMBINED_TOPIC),
            'id': 'combined',
            'description': f"Multiple\n\nFour signals combined into a singel structure",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'struct',
                'members': {
                    'sine': {
                        'type': 'double',
                        'min': 0,
                        'max': 1
                    },
                    'saw': {
                        'type': 'double',
                        'min': 0,
                        'max': 1
                    },
                    'square': {
                        'type': 'double',
                        'min': 0,
                        'max': 1
                    },
                    'noise': {
                        'type': 'double',
                        'min': 0,
                        'max': 1
                    }
                }
            }
        })

        self.links['fast_sine'] = TestLink({
            'value topic': path.join(self.source_prefix, FAST_SINE_TOPIC),
            'id': 'fast_sine',
            'description': f"Fast sine wave\n\nA synthetic sine wave signal with higher frequency",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'array',
                'minlen': FAST_SINE_DATA_POINTS,
                'maxlen': FAST_SINE_DATA_POINTS,
                'members': {
                    'type': 'int',
                    'min': -0,
                    'max': 1
                }
            }
        })

        self.links['trigger_1'] = TestLink({
            'value topic': path.join(self.source_prefix, TRIGGER_1_TOPIC),
            'id': 'trigger_1',
            'description': f"1s trigger pulse\n\nA trigger pulse at 1 second intervals",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'bool'
            }
        })
        self.links['trigger_5'] = TestLink({
            'value topic': path.join(self.source_prefix, TRIGGER_5_TOPIC),
            'id': 'trigger_5',
            'description': f"5s trigger pulse\n\nA trigger pulse at 5 seconds intervals",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'bool'
            }
        })
        self.links['trigger_10'] = TestLink({
            'value topic': path.join(self.source_prefix, TRIGGER_10_TOPIC),
            'id': 'trigger_10',
            'description': f"10s trigger pulse\n\nA trigger pulse at 10 seconds intervals",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'bool'
            }
        })
        self.links['trigger_60'] = TestLink({
            'value topic': path.join(self.source_prefix, TRIGGER_60_TOPIC),
            'id': 'trigger_60',
            'description': f"60s trigger pulse\n\nA trigger pulse at 60 seconds intervals",
            'readonly': True,
            'datasource id': self.id,
            'datainfo': {
                'type': 'bool'
            }
        })


    def update_ntp(self):
        proc = subprocess.Popen(['ntpstat'], stdout = subprocess.PIPE)
        data = proc.communicate()[0].decode('utf-8')
        lines = data.split("\n")
        self.ntp_server = re.search('\((.+)\)', lines[0]).group(1)
        self.ntp_accuracy = int(re.search('(\d+) ms', lines[1]).group(1))
        logging.debug(f"Updated NTP stats: server='{self.ntp_server}', accuracy={self.ntp_accuracy} ms")


    def update_cpu_load(self):
        freq = psutil.cpu_freq()
        self.cpu_frequency = freq.current
        self.cpu_load = psutil.cpu_percent()
        logging.debug(f"Updated CPU load stats: load={self.cpu_load} %, frequency={self.cpu_frequency} MHz")


    def update_temperatures(self):
        sensors = psutil.sensors_temperatures()
        for group_id, group_info in sensors.items():
            for sensor_info in group_info:
                sensor_name = 'temp_' + group_id
                if sensor_info.label:
                    sensor_name = sensor_name + '_' + sensor_info.label.replace(' ', '_')
                self.temperatures[sensor_name] = sensor_info.current
                logging.debug(f"System temperature sensor {sensor_name}={self.temperatures[sensor_name]} C")



if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    ds = TestDatasource(config)
    ds.start()
